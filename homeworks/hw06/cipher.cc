#include "cipher.h"

#define UPPER_CASE(r) ((r) - ('a' - 'A'))

struct Cipher::CipherCheshire {
    string cipherText;
};

Cipher::Cipher()
{
    smile = new CipherCheshire;
    smile->cipherText = "abcdefghijklmnopqrstuvwxyz ";
}
Cipher::Cipher(string in)
{
    smile = new CipherCheshire;
    smile->cipherText = in;
}
string Cipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a';
        } else {
            pos = raw[i] - 'A';
            upper = 1;
        }
        if(upper) {
            retStr += UPPER_CASE(smile->cipherText[pos]);
        } else {
            retStr += smile->cipherText[pos];
        }
    }
    cout << "Done" << endl;

    return retStr;
}

string Cipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here

    // Loop through to find index of current letter
    for (unsigned int i = 0; i < enc.size(); i++) {
    	unsigned int pos;
        for (unsigned int j = 0; j < smile->cipherText.size(); j++) {
        	if (UPPER_CASE(smile->cipherText[j]) == enc[i]) {
        		pos = j;
        	}
        	if (smile->cipherText[j] == enc[i]) {
        		pos = j + 32;
        	}
        }

        // Use position of letter to find new letter
        if (pos == 26 || pos == 58) {
        	retStr += ' ';
        }
        else {
        	retStr += 'A' + pos;
        }
    }

    cout << "Done" << endl;

    return retStr;
}




struct CaesarCipher::CaesarCipherCheshire : CipherCheshire {
     int rot;
};

CaesarCipher::CaesarCipher()
{
    // Fill in code here

    // Initialize to base case values
    CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = "abcdefghijklmnopqrstuvwxyz "; 
    CaesarSmile->rot = 0;

}

CaesarCipher::CaesarCipher(string in, int rot)
{
    // Fill in code here

    // Set to user given values
	CaesarSmile = new CaesarCipherCheshire;
    CaesarSmile->cipherText = in;
    if (rot < 0) {
	    CaesarSmile->rot = (CaesarSmile->cipherText.size() + (rot % 27)) % 27; 
	}
    else {
   		CaesarSmile->rot = rot % 27;
	}
}

string CaesarCipher::encrypt(string raw)
{
    string retStr;
    cout << "Encrypting..." << endl;
    // Fill in code here

    // Find new position of letter according to offset
    for(unsigned int i = 0; i < raw.size(); i++) {
        unsigned int pos;
        bool upper = false;
        if(raw[i] == ' ') {
            pos = 26 + CaesarSmile->rot;
        } else if(raw[i] >= 'a') {
            pos = raw[i] - 'a' + CaesarSmile->rot;
        } else {
            pos = raw[i] - 'A' + CaesarSmile->rot;
            upper = 1;
        }

        // Account for position offset
        pos = pos % 27;

        // Use position of letter to find new letter
        if(upper) {
            retStr += UPPER_CASE(CaesarSmile->cipherText[pos]);
        } else {
            retStr += CaesarSmile->cipherText[pos];
        }
    }

    cout << "Done" << endl;

    return retStr;

}

string CaesarCipher::decrypt(string enc)
{
    string retStr;
    cout << "Decrpyting..." << endl;
    // Fill in code here

    // Loop through to find index of current letter
    for (unsigned int i = 0; i < enc.size(); i++) {
    	unsigned int pos;
        for (unsigned int j = 0; j < CaesarSmile->cipherText.size(); j++) {

            // Case if encrypted letter is capital
        	if (UPPER_CASE(CaesarSmile->cipherText[j]) == enc[i]) {

                // Checks for wrap around letters
                int sign = j - CaesarSmile->rot;
                if (sign < 0) {
                    pos = CaesarSmile->cipherText.size() + sign;
                }
                else {
                    pos = sign;
                }
        	}

            // Case if encrypted letter is lower case
        	if (CaesarSmile->cipherText[j] == enc[i]) {

                // Checks for wrap around letters
                int sign = j - CaesarSmile->rot;
                if (sign < 0) {
                    pos = 32 + CaesarSmile->cipherText.size() + sign;
                }
                else {
                    pos = 32 + sign;
                }
        	}
        }

        // Use position of letter to find new letter
        if (pos == 26 || pos == 58) {
        	retStr += ' ';
        }
        else {
        	retStr += 'A' + pos;
        }
    }

    cout << "Done" << endl;

    return retStr;
}


